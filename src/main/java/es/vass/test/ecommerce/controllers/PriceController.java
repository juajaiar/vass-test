package es.vass.test.ecommerce.controllers;

import es.vass.test.ecommerce.entities.Price;
import es.vass.test.ecommerce.exceptions.PriceException;
import es.vass.test.ecommerce.services.PriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * Rest service for prices
 */
@RestController
@RequestMapping(path = "/prices")
@CrossOrigin("*")
@Slf4j
public class PriceController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private PriceService priceService;

    /**
     * @param startDate date for the appliance´s price, format valid yyyy-MM-dd HH:mm:ss
     * @param productId id product
     * @param brandId   id brand
     * @return price to apply to @productId
     * @throws PriceException
     */
    @GetMapping(path = "/final")
    public ResponseEntity<Price> findByStartDateAndProductIdAndBrandId(@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startDate, @RequestParam("productId") long productId, @RequestParam("brandId") long brandId) throws PriceException {
        return ResponseEntity.ok(priceService.findByStartDateAndProductIdAndBrandId(startDate, productId, brandId));
    }
}

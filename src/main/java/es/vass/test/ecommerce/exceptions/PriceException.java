package es.vass.test.ecommerce.exceptions;

public class PriceException extends Exception{

    public PriceException() {
    }

    public PriceException(String message) {
        super(message);
    }
}

package es.vass.test.ecommerce.entities;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "BRANDS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Brand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BRAND_ID")
    @NotNull
    private Long brandId;

    @Column(name = "NAME")
    @NotNull
    private String name;

    @OneToMany(mappedBy = "brand")
    private List<Product> products;

}
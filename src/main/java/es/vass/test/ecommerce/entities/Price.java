package es.vass.test.ecommerce.entities;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "PRICES")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRICES_ID")
    @NotNull
    private Long priceId;

    @Column(name = "BRAND_ID")
    @NotNull
    private Long brandId;

    @Column(name = "START_DATE")
    @NotNull
    private Date startDate;

    @Column(name = "END_DATE")
    @NotNull
    private Date endDate;

    @Column(name = "PRICE_LIST")
    @NotNull
    private Integer priceList;

    @Column(name = "PRODUCT_ID")
    @NotNull
    private Long productId;

    @Column(name = "PRIORITY")
    @NotNull
    private Boolean priority;

    @Column(name = "PRICE")
    @NotNull
    private Float price;

    @Column(name = "CURR")
    @NotNull
    private String curr;

}
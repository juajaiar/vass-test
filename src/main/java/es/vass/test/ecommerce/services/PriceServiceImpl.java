package es.vass.test.ecommerce.services;

import es.vass.test.ecommerce.entities.Price;
import es.vass.test.ecommerce.exceptions.PriceException;
import es.vass.test.ecommerce.repositories.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class PriceServiceImpl implements PriceService {

    @Autowired
    private PriceRepository priceRepository;

    @PersistenceContext
    private EntityManager entityManager;
    private CriteriaBuilder criteriaBuilder;

    private CriteriaQuery<Price> criteriaQuery;

    private List<Predicate> predicates = new ArrayList<>();

    private Root<Price> root;

    private TypedQuery<Price> query;

    @PostConstruct
    private void postConstruct() {
        this.criteriaBuilder = entityManager.getCriteriaBuilder();
        this.criteriaQuery = criteriaBuilder.createQuery(Price.class);
        this.root = criteriaQuery.from(Price.class);
    }

    /**
     * @param startDate date for the appliance´s price
     * @param productId id product
     * @param brandId   id brand
     * @return price to apply to @productId
     * @throws PriceException
     */
    @Override
    public Price findByStartDateAndProductIdAndBrandId(Date startDate, Long productId, Long brandId) throws PriceException {
        try {
            predicates.clear();
            ParameterExpression<Date> dateExpression = criteriaBuilder.parameter(Date.class);
            predicates.add(criteriaBuilder.between(dateExpression, root.<Date>get("startDate"), root.<Date>get("endDate")));
            predicates.add(criteriaBuilder.equal(root.get("brandId"), brandId));
            predicates.add(criteriaBuilder.equal(root.get("productId"), productId));
            criteriaQuery.select(root).where(predicates.toArray(new Predicate[]{}));
            this.query = entityManager
                    .createQuery(criteriaQuery)
                    .setParameter(dateExpression, startDate, TemporalType.DATE);
            return this.query.getResultList()//
                    .stream()//
                    .max(Comparator.comparing(Price::getPriceList))//
                    .orElseThrow(() -> new PriceException());//
        } catch (PriceException e) {
            throw e;
        } catch (Exception e) {
            throw new PriceException(e.getMessage());
        }
    }
}
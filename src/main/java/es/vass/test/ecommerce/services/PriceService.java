package es.vass.test.ecommerce.services;

import es.vass.test.ecommerce.entities.Price;
import es.vass.test.ecommerce.exceptions.PriceException;

import javax.persistence.NoResultException;
import java.util.Date;

public interface PriceService {


    /**
     * @param startDate date for the appliance´s price
     * @param productId id product
     * @param brandId   id brand
     * @return price to apply to @productId
     * @throws PriceException
     */
    Price findByStartDateAndProductIdAndBrandId(Date startDate, Long productId, Long brandId) throws PriceException;
}

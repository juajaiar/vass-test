package es.vass.test.ecommerce.repositories;


import es.vass.test.ecommerce.entities.Brand;
import es.vass.test.ecommerce.entities.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product,Long> {}

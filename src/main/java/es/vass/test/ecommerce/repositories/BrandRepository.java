package es.vass.test.ecommerce.repositories;


import es.vass.test.ecommerce.entities.Brand;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepository extends PagingAndSortingRepository<Brand,Long> {}

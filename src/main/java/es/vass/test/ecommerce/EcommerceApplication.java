package es.vass.test.ecommerce;

import es.vass.test.ecommerce.repositories.PriceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping(path = "/test")
@Slf4j
public class EcommerceApplication implements CommandLineRunner {

    @Autowired
    private PriceRepository priceRepository;

    public static void main(String[] args) {
        SpringApplication.run(EcommerceApplication.class, args);
    }


    /**
     * is app running?
     *
     * @return if the app is running, ping message valid, else null/empty
     */
    @GetMapping
    public ResponseEntity<String> test() {
        return ResponseEntity.ok("\"Ecommerce is running!\"");
    }

    /**
     * Verify insert db
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        priceRepository
                .findAll()
                .stream()
                .forEach(price -> log.info(price.toString()));
    }
}
